<?php
/**
 * Bootstrapping unit test framework
 *
 */

require_once __dir__ . '/../../boot.php';
require_once __dir__ . '/UnitTestCase.php';
require_once __dir__ . '/Module/TestCase.php';
